/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#include "ds_ros_examples/ros_example_dptr.h"
#include "ros_example_dptr_private.h"

// It may be helpful to define timeouts and delays for an IOSM etc.
#define EXAMPLE_TIMEOUT 1.5
#define EXAMPLE_DELAY 0.5


using namespace ds_ros_examples;

RosExampleDriver::RosExampleDriver() : ds_base::DsProcess(),
d_ptr_(std::unique_ptr<RosExampleDriverPrivate>(new RosExampleDriverPrivate))
{
}

RosExampleDriver::RosExampleDriver(int argc, char* argv[], const std::string& name) : ds_base::DsProcess(argc, argv, name),
                                                                                  d_ptr_(std::unique_ptr<RosExampleDriverPrivate>(new RosExampleDriverPrivate))
{
}

RosExampleDriver::~RosExampleDriver() = default;

///*--------------------*///
///*  SETUP FUNCTIONS  *///
///*-------------------*///
void RosExampleDriver::setupTimers() {
    // This creates a timer for every 5 seconds and attaches it to the timerCallback function
    ds_base::DsProcess::setupTimers();
    DS_D(RosExampleDriver);
    auto nh = nodeHandle();
    d->timer_ = nh.createTimer(ros::Duration(5.0), &RosExampleDriver::timerCallback, this);
}

void RosExampleDriver::setupParameters()
{
    ds_base::DsProcess::setupParameters();
    DS_D(RosExampleDriver);
    auto nh = this->nodeHandle();
    // Any topics here should echo what exists in "ros_example_private.h" and what is defined in the config yaml file
    d->example_status_topic_ = ros::param::param<std::string>("~example_status_topic", "statusMsg");

    //Setup any parameters you need from the param server. These should be stored in variables defined in "ros_example_private.h" and set in the config yaml file
    nh.getParam(d->nodeName + "/" + "descriptive_name", d->descriptive_name);
    nh.getParam(d->nodeName + "/" + "example_param", d->example_param);
    ROS_INFO_STREAM("params set");

}

void RosExampleDriver::setupConnections() {
    ds_base::DsProcess::setupConnections();
    DS_D(RosExampleDriver);
    auto nh = nodeHandle();
    
    // Setup any connections needed to the sensor/device
    // This can be done a number of different ways, most commonly with an IO State Machine or an AddConnection call
    
    // For an IOSM:
    d->example_iosm_conn_ = addIoSM("statemachine", "example_iosm_connection");

    // For a standard connection w/ a callback to parseReceivedBytes:
    d->example_conn_ = addConnection("example_connection", boost::bind(&RosExampleDriver::parseReceivedBytes, this, _1));

    ROS_INFO_STREAM("connections set");
}

void RosExampleDriver::setupServices() {
    ds_base::DsProcess::setupServices();
    DS_D(RosExampleDriver);
    auto nh = nodeHandle();

    // Should correspond with the services in "ros_example_private.h" and listed in the yaml config file
    std::string example_srv_ = ros::param::param<std::string>("~example_service", "exampleCmd");

    //Now advertise the service and bind it to the exampleCmd we created previously
    d->example_srv_ = nh.advertiseService<ds_core_msgs::VoidCmd::Request, ds_core_msgs::VoidCmd::Response>
                (d->nodeName + "/" + example_srv_, boost::bind(&RosExampleDriver::exampleCmd, this, _1, _2));
    
    ROS_INFO_STREAM("services set");
}


void RosExampleDriver::setupPublishers() {
    ds_base::DsProcess::setupPublishers();
    DS_D(RosExampleDriver);
    // prepare our publishers
    auto nh = nodeHandle();

    //Setup any publishers, should be consistent with "ros_example_private.h" and yaml config file
    d->example_status_pub_ = nh.advertise<ds_hotel_msgs::HTP>(d->nodeName + "/" + d->example_status_topic_, 10);

    ROS_INFO_STREAM("publishers set");
}

void RosExampleDriver::setupSubscriptions() {
    ds_base::DsProcess::setupSubscriptions();
    DS_D(RosExampleDriver);
    auto nh = nodeHandle();
    // Setup any subscribers, again consistent with "ros_example_private.h" and yaml config file
    d->example_sub_ = nh.subscribe(d->nodeName + "/" + d->example_status_topic_, 10, &RosExampleDriver::subscriberCallback, this);
    ROS_INFO_STREAM("subscribers set");
}
///*---------------------*///
///*  CALLBACK FUNCTIONS  *///
///*---------------------*///
// Parse bytes and pass to msg parsers
bool RosExampleDriver::parseReceivedBytes(const ds_core_msgs::RawData& bytes) {
    DS_D(RosExampleDriver);
    /*
    parseReceivedBytes is the callback for raw data from the sensor/device. 
    You can either parse data directly in this function or, more likely, use this function as a gateway to other parsers as below. 
    For instance, you can use parseReceivedBytes to call a header parser that parses just your data header and checks for validity
    and/or data type before parsing individual messages.
    */
    if (!parseMessage(bytes)) {
        ROS_ERROR_STREAM("EXAMPLE PARSE FAILED!");
        return false;
    }
    return true;
}

void RosExampleDriver::subscriberCallback(const ds_hotel_msgs::HTP& msg) {
    DS_D(RosExampleDriver);
    /* 
    This is the callback for the subscriber defined above.
    This should take in that data (in this case, HTP data) and call other functions
    and/or send it somewhere as appropriate. 
    
    For instance, you might subscribe to HTP data and send a device the "OFF" command if the
    Temp is too high. In this case, we would call the sendExampleQuery or sendExampleCmd functions.
    */
}
bool RosExampleDriver::parseMessage(const ds_core_msgs::RawData& bytes) {
    DS_D(RosExampleDriver);

    //It's often pretty useful to reinterpret the data from the raw bytes
    auto msg = std::string{reinterpret_cast<const char*>(bytes.data.data()), bytes.data.size() };

    // Some sort of parsing logic should go here and return false if errors are found or parsing fails
        return true;
}

///*---------------------*///
///*  COMMAND FUNCTIONS  *///
///*---------------------*///
// send example command to device
bool RosExampleDriver::exampleCmd(ds_core_msgs::VoidCmd::Request& req, ds_core_msgs::VoidCmd::Response& res) {
    DS_D(RosExampleDriver);
}

///*---------------------------*///
///*  QUERY/TIMEOUT FUNCTIONS  *///
///*---------------------------*///
void RosExampleDriver::timerCallback(const ros::TimerEvent&) {
    DS_D(RosExampleDriver);
    ROS_WARN_STREAM_ONCE("STARTING Timer Callback Function");
    //This Function gets called via the ROS timer, so it's a good place to do some querying or device timeout checking
}

std::string RosExampleDriver::sendExampleQuery(std::string descriptive_name, int example_param) {
    DS_D(RosExampleDriver);
    /*Create the appropriate query command and then add to the previously defined IOSM
    In the examples below, we use parseReceivedBytes for both status queries and cmds.
    This is not necessary as any callback function can be used and is just for example purposes.
    For more info on I/O state machines, see "IO_STATE_MACHINE.md" in ds_base/ds_asio. 
    */
    std::string statusstr = "";
    auto status_cmd = ds_asio::IoCommand(statusstr, EXAMPLE_TIMEOUT, false, boost::bind(&RosExampleDriver::parseReceivedBytes, this, _1));
    status_cmd.setDelayAfter(ros::Duration(EXAMPLE_DELAY));
    d->example_iosm_conn_->addRegularCommand(std::move(status_cmd));
    return statusstr;
}
// Send commands to correct power switch card
std::string RosExampleDriver::sendExampleCmd(std::string descriptive_name, int example_param) {
    DS_D(RosExampleDriver);
    /* Create the appropriate command for the device and then add to the previously defined IOSM
       In this example, we use a PreemptCommand for the actual cmd immediately followed by a status query preempt.
       Generally, it's good practice to check the status change soon after sending a command, which is why we insert
       the additional preempt.
    */
    std::string cmdstr = "";
    std::string statusstr = sendExampleQuery(descriptive_name, example_param);
    auto example_cmd = ds_asio::IoCommand(cmdstr, EXAMPLE_TIMEOUT, false, boost::bind(&RosExampleDriver::parseReceivedBytes, this, _1));
    auto status_cmd = ds_asio::IoCommand(statusstr, EXAMPLE_TIMEOUT, false, boost::bind(&RosExampleDriver::parseReceivedBytes, this, _1));
    example_cmd.setDelayAfter(ros::Duration(EXAMPLE_DELAY));
    d->example_iosm_conn_->addPreemptCommand(std::move(example_cmd));
    d->example_iosm_conn_->addPreemptCommand(std::move(status_cmd));
    return cmdstr;
}
