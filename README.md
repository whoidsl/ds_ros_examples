## Example ROS Drivers incorporating some of the major DS_ROS Features

Note: This repo contains multiple ROS nodes. There is currently a node utilizing dptr's and a node that just uses a derived class. Efforts have been made to clearly label which files belong to which node via "_dptr" and "_derived" file naming schemas. For more info on what a D-Pointer is and why you may wish to use it over a derived class implementation, [see this article](https://wiki.qt.io/D-Pointer).

These drivers are intended as an example device driver with some of the typical ds_ros characteristics. Namely, each utilizes:

* A Yaml file for configuration

* Subclassing DsProcess

* An I/O State Machine connection and a DsConnection to the "device"

* A parseReceivedBytes callback for RawData from the "device"

* Other features characteristic of many of the drivers currently in the ds_ros codebase.

The comments are intended to describe the major purpose of each function etc. as well as point people to other helpful documents. Of these, I have found [CONNECTION_TYPES](https://bitbucket.org/whoidsl/ds_base/src/master/ds_asio/CONNECTION_TYPES.md), [I/O State Machine](https://bitbucket.org/whoidsl/ds_base/src/master/ds_asio/IO_STATE_MACHINE.md), and the [ds_sensors extending doc](https://bitbucket.org/whoidsl/ds_sensors/src/master/EXTENDING.md) to be extremely useful. 

If you would like to contribute to this repo, feel free to add an issue or submit a PR. You can also [email me](mailto:ivandor@whoi.edu).