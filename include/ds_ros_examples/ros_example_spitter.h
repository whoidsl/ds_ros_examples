/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivandor on 12/15/20.
//

#ifndef PROJECT_DS_ROS_EXAMPLE_H
#define PROJECT_DS_ROS_EXAMPLE_H

#include <ds_base/ds_process.h>
#include <ds_base/util.h>
#include <string>
#include <memory>
// Include whatever ROS msgs/srvs you need or develop for this driver here
// FOR EXAMPLE:
#include <ds_hotel_msgs/HTP.h>
#include <ds_core_msgs/VoidCmd.h>

// Setup your namespace
namespace ds_ros_examples
{
struct RosExampleDriverPrivate;

//A pretty typical ROS driver will subclass DsProcess. Sensor drivers typically subclass SensorBase. See ds_base for more info on some of the useful base classes.
class RosExampleDriver : public ds_base::DsProcess
{
    DS_DECLARE_PRIVATE(RosExampleDriver) // Declare private so we can store most of the implementation variables etc in the ros_example_private.h file

public:
  explicit RosExampleDriver();
  RosExampleDriver(int argc, char* argv[], const std::string& name);
  ~RosExampleDriver() override;
  DS_DISABLE_COPY(RosExampleDriver)

// We will end up overriding a lot of the default DsProcess setup functions
protected:
  /// \function overrides from DsProcess
  void setupParameters() override;
  void setupConnections() override;
  void setupServices() override;
  void setupPublishers() override;
  void setupSubscriptions() override;
  void setupTimers() override;

// The primary functions for callbacks/parsing/cmds from the device
  bool parseReceivedBytes(const ds_core_msgs::RawData& bytes);
  bool parseMessage(const ds_core_msgs::RawData& bytes);
  void subscriberCallback(const ds_hotel_msgs::HTP& msg);
  void timerCallback(const ros::TimerEvent& event);
  bool exampleCmd(ds_core_msgs::VoidCmd::Request& req, ds_core_msgs::VoidCmd::Response& res);

  std::string sendExampleQuery(std::string descriptive_name, int example_param);
  std::string sendExampleCmd(std::string descriptive_name, int example_param);
private:
    std::unique_ptr<RosExampleDriverPrivate> d_ptr_;


};
} //namespace ds_ros_examples

#endif  // PROJECT_DS_ROS_EXAMPLE_H
