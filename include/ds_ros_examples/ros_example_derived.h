/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivandor on 12/15/20.
//

#ifndef PROJECT_DS_ROS_EXAMPLE_H
#define PROJECT_DS_ROS_EXAMPLE_H

#include <ds_base/ds_process.h>
#include <ds_base/util.h>
#include <string>
#include <memory>
// Include whatever ROS msgs/srvs you need or develop for this driver here
// FOR EXAMPLE:
#include <ds_hotel_msgs/HTP.h>
#include <ds_core_msgs/VoidCmd.h>

// Setup your namespace
namespace ds_ros_examples
{
  struct RosExampleDerivedDriverPrivate;

  //A pretty typical ROS driver will subclass DsProcess. Sensor drivers typically subclass SensorBase. See ds_base for more info on some of the useful base classes.
  class RosExampleDerivedDriver : public ds_base::DsProcess
  {
  public:
    explicit RosExampleDerivedDriver();
    RosExampleDerivedDriver(int argc, char *argv[], const std::string &name);
    ~RosExampleDerivedDriver() override;
    DS_DISABLE_COPY(RosExampleDerivedDriver)

    // We will end up overriding a lot of the default DsProcess setup functions
  protected:
    /// \function overrides from DsProcess
    void setupParameters() override;
    void setupConnections() override;
    void setupServices() override;
    void setupPublishers() override;
    void setupSubscriptions() override;
    void setupTimers() override;

    // The primary functions for callbacks/parsing/cmds from the device
    bool parseReceivedBytes(const ds_core_msgs::RawData &bytes);
    bool parseMessage(const ds_core_msgs::RawData &bytes);
    void subscriberCallback(const ds_hotel_msgs::HTP &msg);
    void timerCallback(const ros::TimerEvent &event);
    bool exampleCmd(ds_core_msgs::VoidCmd::Request &req, ds_core_msgs::VoidCmd::Response &res);

    std::string sendExampleQuery(std::string descriptive_name, int example_param);
    std::string sendExampleCmd(std::string descriptive_name, int example_param);

  private:
    /// \brief ROS Services loaded from param server
    ros::ServiceServer example_srv_;

    // \brief Publishers from param server
    ros::Publisher example_status_pub_;

    // \brief Subscribers
    ros::Subscriber example_sub_;

    // \brief Timers
    ros::Timer timer_;

    // \brief Messages
    ds_hotel_msgs::HTP example_msg;

    // \brief Topics
    std::string example_status_topic_;

    //Driver specific params
    std::string descriptive_name;
    int example_param;

    // These are useful variables for setting up topic names etc.
    const std::string nodeName = ros::this_node::getName();
    const std::string nameSpace = ros::this_node::getNamespace();

    // Connections to the device
    /* Once is a DsConnection and one is an IoSM. 
        In Practice, you may not need both, but they are included here as an example. 
        The connection type depends on how you want the driver to function. 
        See ds_base/ds_asio "CONNECTION_TYPES.md" for more info.
        */
    /// \brief I/O State Machine Connection
    boost::shared_ptr<ds_asio::IoSM> example_iosm_conn_;

    /// \brief TCP Connection Example
    boost::shared_ptr<ds_asio::DsConnection> example_conn_;
  };

} //namespace ds_ros_examples

#endif // PROJECT_DS_ROS_EXAMPLE_H
